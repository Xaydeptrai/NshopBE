﻿using Microsoft.AspNetCore.Mvc;
using NSHOP.Application.Interfaces.IService;
using NSHOP.Domain;

namespace NSHOP.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductService _service;

        public ProductController(IProductService service)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult<List<Product>> Get()
        {
            var productsFromService = _service.GetAllProduct();
            return Ok(productsFromService);
        }

        [HttpPost]
        public ActionResult<Product> PostProduct(Product product)
        {
            var Proudct = _service.CreateProduct(product);
            return Ok(product);
        }
    }
}
