﻿using NSHOP.Application.Interfaces.IRepositiory;
using NSHOP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSHOP.Infrastructure.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ProductDbContext _productDbContext;
        public ProductRepository(ProductDbContext productDbContext)
        {
            _productDbContext = productDbContext;
        }
        public Product CreateProduct(Product product)
        {
            _productDbContext.Products.Add(product);
            _productDbContext.SaveChanges();

            return product;
        }

        public List<Product> GetAllProduct()
        {
            return _productDbContext.Products.ToList();
        }
    }
}
