﻿using Microsoft.EntityFrameworkCore;
using NSHOP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSHOP.Infrastructure
{
    public class ProductDbContext : DbContext
    {
        public ProductDbContext(DbContextOptions<ProductDbContext> options) : base(options) 
        {

        }
        public DbSet<Product> Products { get; set; }
    }
}
