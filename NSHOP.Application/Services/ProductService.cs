﻿using NSHOP.Application.Interfaces.IRepositiory;
using NSHOP.Application.Interfaces.IService;
using NSHOP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSHOP.Application.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public Product CreateProduct(Product product)
        {
            _productRepository.CreateProduct(product);
            return product;
        }

        public List<Product> GetAllProduct()
        {
            var products = _productRepository.GetAllProduct();
            return products;
        }
    }
}
