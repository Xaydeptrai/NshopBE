﻿using NSHOP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSHOP.Application.Interfaces.IRepositiory
{
    public interface IProductRepository
    {
        List<Product> GetAllProduct();
        Product CreateProduct(Product product);
    }
}
