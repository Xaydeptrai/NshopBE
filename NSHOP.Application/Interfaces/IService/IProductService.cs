﻿using NSHOP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSHOP.Application.Interfaces.IService
{
    public interface IProductService
    {
        List<Product> GetAllProduct();

        Product CreateProduct(Product product);
    }
}
